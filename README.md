## Instructions

This part of the interview process allows us to learn more about your software engineering and web development skills. Below is a description of a CRUD API that manages products, keeps track of their pricing and their view counts. You are given a boilerplate application with parts that are incomplete or not working as expected. The task is to add features to the application, adapt some parts of it and come prepared at the next interview with suggestions on how to improve it.

The boilerplate application has some basic components set up: a Product model with a database connection and an empty controller. We would like you to do the following:
- Add an API to get a single product
- Finish the implementation for fetching the currency conversion

When a single product is requested, all fields of that product are returned and the view-count for that product is incremented. The request can optionally specify a currency, in which case the price should be converted to the requested currency before being returned. We need to support the following currencies:
*	USD (default)
*	CAD
*	EUR
*	GBP

The latest exchange rates are retrieved from the public API https://currencylayer.com/. Tests are optional but we would like to hear from you how would you design such tests at the interview.


## Implementation

### Project Structure
- The project is structured in classical gradle java-application style.
    
- Folder 'init' is used for keeping files needed during application deployment 
      (at this moment it contains SQL scrips for creating Product db with data)

- Docker files in the root directory are used for building the app's image and deploying the containers. 
Also, ENV dependent variables are passed through them.

### Project Deployment

1. Build jar: `gradle bootJar`
2. Pack it into image: `docker build -t product-manager-api .`
3. (Re)-run docker-compose: `docker-compose up`

### Endpoints
The main app is deployed on port `spring.port` in `/config/application-*.yml`, it gives access to:
 - swagger-spec: `/swagger-ui-custom.html` 
 - product-endpoints: `/api/product/*`

The management port is `management.server.port`, where Actuator's endpoints are available.
* As a further improvement, the admin port might be secured (not available from outside) 
or provide any useful api to get more insight about the application state.

### Product Service

#### Product Entity Fields
- **name**: limited by length, not null
- **description**: limited by length
- **price**: double, not null
- **currency**: string, not null (**NOTE**: for keeping the price currency at the moment of an entity creation, use case: user wants to create a product in ANY supported currency)
- **views_count**: int, not null
- **deleted**: boolean, not null (if _false_, a record will not be accessible through API calls)

#### GET product
1. If product doesn't exist OR exists, but its deleted=true => return 404 with additional error code and info
2. If product exists AND deleted=false:
   1. in a separate thread: increment the record's _views_count_
   2. if requested to change currency:
      1. _from == to_ => return price without conversion
      2. _from != to_ => try to convert
         1. if failed => in a separate thread notify user about the problem with conversion, in the main thread - return the original price
         2. if ok => update a DTO's price and currency and return.
   
#### CREATE product
1. Create a product and return created ID

#### DELETE product
1. Return _**NO_CONTENT**_ in all cases, in order not to expose info about product existence.

### Notification Sender
Suggested as improvement: In case **_Currency service_** is unavailable, 
we still can return a requested product, but without currency conversion.
In order to inform a user, that we have noticed the problem, we might send a pop-up notification to the client's ui

### Conversion Service
Uses https://currencylayer.com/ for receiving currency quotes and converting currencies.
Provides currency conversion functionality: 
1. Conversion flow
   * calls `/convert` endpoint 
     * if failed => calls `/live` endpoint and requests currency quotes
       * if failed => throws exception 
       * if ok => returns conversion
     * if ok => returns conversion
     
2. Further improvements
   If `/convert` functionality cannot be purchased, we still might need to upgrade the account in order not to exceed
   subscription limits on requesting `/live` exchanges. 
   However, we might reduce amount of `/life` calls by:
   1. in a separate flow, defining a periodical quotes updates
   2. introducing caching (at least a light-weight in-memory cache) and storing quotes in it
   3. having all above allows us to get rid of `/convert` calls and might prevent from exceeding subscription limits.

#### Error Handling

1. **Global Error handler**. Error codes can give more insight about the exceptional situation
   by supporting conventional list of error codes. That is helpful when, for instance, receiving 404 HTTP status, 
   we can distinguish situations of `(http) Resource not found` or `Product not found`.
2. **Async Error handler** For the _fire-and-forget_ flows, it's still important to monitor what happens.


### What's next
1. Define such API functions as `update`, `getAll`, `deleteAll`, `updateAll`
2. Implement caching layer for currency quotes
3. Find out satisfying DB connection pool parameters 
4. Find out thread pools configs 
5. Monitoring and logging are further improvements for the service
6. Isolated component testing in the environment with mocked dependencies and real db (e.g. created by docker-compose)
7. E2E testing in staging environment