FROM adoptopenjdk/openjdk8
ARG JAR_FILE=build/libs/product-manager-api-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} product-manager-api.jar
ENTRYPOINT ["java", "-jar", "product-manager-api.jar", "--spring.profiles.active=${ENV}"]