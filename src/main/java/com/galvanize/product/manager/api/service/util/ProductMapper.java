package com.galvanize.product.manager.api.service.util;

import com.galvanize.product.manager.api.domain.Product;
import com.galvanize.product.manager.api.model.ProductDTO;
import lombok.NonNull;

public final class ProductMapper {

    private ProductMapper() {
    }

    public static Product convert(@NonNull ProductDTO product) {
        final Product p = new Product();
        p.setName(product.getName());
        p.setDescription(product.getDescription());
        p.setPrice(product.getPrice());
        p.setCurrency(product.getCurrency());
        return p;
    }

    public static ProductDTO convert(@NonNull Product product) {
        final ProductDTO p = new ProductDTO();
        p.setId(product.getId());
        p.setName(product.getName());
        p.setDescription(product.getDescription());
        p.setPrice(product.getPrice());
        p.setCurrency(product.getCurrency());
        return p;
    }
}
