package com.galvanize.product.manager.api.repository;

import com.galvanize.product.manager.api.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Modifying
    @Transactional
    @Query("update Product p set p.viewsCount = p.viewsCount + 1 where p.id = :productId and p.deleted=false")
    @Async(value = "subtask-executor")
    void incrementProductViews(@Param("productId") Integer productId);

    @Query("select p from Product p where p.id = :productId and p.deleted = false")
    @Override
    Optional<Product> findById(@Param("productId") Integer productId);

    @Modifying
    @Transactional
    @Query("update Product p set p.deleted = true where p.id = :productId and p.deleted=false")
    @Override
    void deleteById(@Param("productId") Integer productId);
}
