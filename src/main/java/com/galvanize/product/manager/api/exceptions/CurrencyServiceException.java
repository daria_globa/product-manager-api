package com.galvanize.product.manager.api.exceptions;

public class CurrencyServiceException extends RuntimeException {

    public CurrencyServiceException(String msg) {
        super(msg);
    }

    public CurrencyServiceException(Throwable e) {
        super(e);
    }
}
