package com.galvanize.product.manager.api.domain;

import com.galvanize.product.manager.api.model.Currency;
import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Data
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "primary_sequence")
    @SequenceGenerator(name = "primary_sequence", sequenceName = "primary_sequence", allocationSize = 1)
    private Integer id;

    @Column(nullable = false, length = 50)
    private String name;

    @Column(length = 255)
    private String description;

    @Column(nullable = false, precision = 10, scale = 2)
    private double price;

    @Column(name = "views_count", nullable = false)
    @ColumnDefault("0")
    private int viewsCount;

    @Column(nullable = false)
    @ColumnDefault("false")
    private boolean deleted;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Currency currency;

    @Version
    private Integer version;
}

