package com.galvanize.product.manager.api.config;

import com.galvanize.product.manager.api.repository.ProductRepository;
import com.galvanize.product.manager.api.service.NotificationSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

@Slf4j
@EnableAsync
@Configuration
public class AsyncSubtaskExecutionConfig implements AsyncConfigurer {

    @Value("${thread-pool-executor.subtask.core-pool-size}")
    private int corePoolSize;

    @Value("${thread-pool-executor.subtask.max-pool-size}")
    private int maxPoolSize;

    @Value("${thread-pool-executor.subtask.queue-size}")
    private int queueSize;

    @Primary
    @Bean(name="subtask-executor")
    public Executor getAsyncExecutor() {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueSize);
        executor.setThreadNamePrefix("subtask-");
        executor.initialize();
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return (throwable, method, params) -> {
            final String clazz = method.getDeclaringClass().getCanonicalName();

            if (clazz.equalsIgnoreCase(NotificationSender.class.getCanonicalName())) {
                /* update monitoring metrics for NotificationService */
            } else if (clazz.equalsIgnoreCase(ProductRepository.class.getCanonicalName())) {
                /* update monitoring metrics for ProductRepository */
            }

            log.error("Subtask {} failed to execute with parameters: {}, exception:\n{}",
                    clazz + "." + method.getName(),
                    Arrays.stream(params).map(Object::toString).collect(Collectors.joining()),
                    throwable);
        };
    }
}
