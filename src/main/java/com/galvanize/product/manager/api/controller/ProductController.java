package com.galvanize.product.manager.api.controller;

import com.galvanize.product.manager.api.model.Currency;
import com.galvanize.product.manager.api.model.ProductDTO;
import com.galvanize.product.manager.api.service.ProductService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(
        value = "/api/product",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

    @Autowired
    private final ProductService productService;

    public ProductController(final ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/{productId}")
    @ResponseBody
    @Timed("product.get-one")
    public ResponseEntity<ProductDTO> getProduct(@PathVariable Integer productId,
                                                 @RequestParam(required = false) Currency cur) {
        return ResponseEntity.ok(productService.get(productId, Optional.ofNullable(cur)));
    }

    @PostMapping("/new")
    @ResponseBody
    @Timed("product.create-one")
    public ResponseEntity<ProductDTO> newProduct(@Valid @RequestBody ProductDTO product) {
        return ResponseEntity.ok(productService.create(product));
    }

    @DeleteMapping("/{productId}")
    @ResponseStatus
    public ResponseEntity<?> delete(@PathVariable Integer productId) {
        productService.delete(productId);
        return ResponseEntity.noContent().build();
    }
}
