package com.galvanize.product.manager.api.model.currency;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.galvanize.product.manager.api.model.Currency;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyQuotes implements CurrencyModel {

    private boolean success;
    private Currency source;
    private Map<Currency, Double> quotes;
    private Error error;

    @JsonProperty("quotes")
    public void setQuotes(final Map<String, Double> rawQuotes) {
        this.quotes = new HashMap<Currency, Double>() {{
            for (String cur : rawQuotes.keySet()) {
                final String rawCur = cur.replace(source.name(), "");
                final Currency key = Currency
                        .byName(rawCur)
                        .orElseThrow(() -> new IllegalArgumentException("Unsupported currency: " + rawCur));
                put(key, rawQuotes.get(cur));
            }
        }};
    }

    public double quote(Currency cur) {
        return quotes.get(cur);
    }

    public static CurrencyQuotes error(Throwable t) {
        final CurrencyQuotes o = new CurrencyQuotes();
        o.setError(new Error(t));
        o.setSuccess(false);

        return o;
    }

    @Override
    public boolean succeed() {
        return success && error == null;
    }

    @Override
    public Error error() {
        return error;
    }
}
