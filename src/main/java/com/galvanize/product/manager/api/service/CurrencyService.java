package com.galvanize.product.manager.api.service;

import com.galvanize.product.manager.api.exceptions.CurrencyServiceException;
import com.galvanize.product.manager.api.model.Currency;
import com.galvanize.product.manager.api.model.currency.CurrencyConversion;
import com.galvanize.product.manager.api.model.currency.CurrencyQuotes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Optional;


@Service
@Slf4j
public class CurrencyService {

    @Value("${external.currency.exchange.api.api-key}")
    private String apiKey;

    @Value("${external.currency.exchange.api.convert-path}")
    private String convertPath;

    @Value("${external.currency.exchange.api.quotes-path}")
    private String quotesPath;

    private final WebClient csWebClient;

    public CurrencyService(final WebClient webClient) {
        this.csWebClient = webClient;
    }

    public double convert(double amount, Currency from, Currency to) {
        final CurrencyConversion conversion = convertDirectly(amount, from, to);

        if (conversion.succeed())
            return conversion.getResult();
        else
            log.warn("Will try to convert currency using direct conversion, because of error: {}", conversion.error());

        final CurrencyQuotes quotes = convertThroughQuotesRequest(from, to);
        if (quotes.succeed())
            return quotes.quote(to) * amount;
        else {
            log.error("Failed to convert currency using quotes, because of error: {}", quotes.error());
            throw Optional.ofNullable(quotes.error())
                    .filter(error -> error.getCause() != null && error.getCause() instanceof CurrencyServiceException)
                    .map(error -> (CurrencyServiceException) error.getCause())
                    .orElseGet(() -> new CurrencyServiceException("Failed to convert currency using API"));
        }
    }

    /**
     * Converts amount from one currency to another
     * <p>
     * See contract of https://api.currencylayer.com/convert?access_key=YOUR_ACCESS_KEY&from={from}&to={to}&amount={amount}
     */
    protected CurrencyConversion convertDirectly(double amount, Currency from, Currency to) {
        return Optional
                .ofNullable(csWebClient.get()
                        .uri(builder -> builder
                                .path(convertPath)
                                .queryParam("access_key", apiKey)
                                .queryParam("from", from)
                                .queryParam("to", to)
                                .queryParam("amount", amount)
                                .build())
                        .accept(MediaType.APPLICATION_JSON)
                        .retrieve()
                        .onStatus(HttpStatus::isError, resp -> serviceErrorResponse(convertPath, resp))
                        .bodyToMono(CurrencyConversion.class)
                        .onErrorResume(t -> Mono.just(CurrencyConversion.error(t)))
                        .block())
                .orElseGet(() -> CurrencyConversion.error(new CurrencyServiceException("No data from " + convertPath)));
    }

    protected CurrencyQuotes convertThroughQuotesRequest(Currency from, Currency to) {
        return Optional
                .ofNullable(csWebClient.get()
                        .uri(builder -> builder
                                .path(quotesPath)
                                .queryParam("access_key", apiKey)
                                .queryParam("source", from)
                                .queryParam("currencies", to)
                                .build())
                        .accept(MediaType.APPLICATION_JSON)
                        .retrieve()
                        .onStatus(HttpStatus::isError, resp -> serviceErrorResponse(quotesPath, resp))
                        .bodyToMono(CurrencyQuotes.class)
                        .block())
                .orElseGet(() -> CurrencyQuotes.error(new CurrencyServiceException("No data from " + quotesPath)));
    }

    private static <T> Mono<T> serviceErrorResponse(String path, ClientResponse resp) {
        return Mono.error(new CurrencyServiceException(path + " responded with error code " + resp.statusCode()));
    }
}