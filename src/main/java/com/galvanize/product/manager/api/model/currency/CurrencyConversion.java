package com.galvanize.product.manager.api.model.currency;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyConversion implements CurrencyModel {

    private boolean success;
    private double result;
    private Error error;

    public static CurrencyConversion error(Throwable t) {
        final CurrencyConversion o = new CurrencyConversion();
        o.setError(new Error(t));
        o.setSuccess(false);

        return o;
    }

    @Override
    public boolean succeed() {
        return success && error == null;
    }

    @Override
    public Error error() {
        return error;
    }
}
