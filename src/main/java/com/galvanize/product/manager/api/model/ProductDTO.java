package com.galvanize.product.manager.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class ProductDTO {

    @Schema(type = "int", example = "your product id")
    private Integer id;

    @NotNull
    @Size(min = 1, max = 50)
    @Schema(type = "string", example = "your product name")
    private String name;

    @Size(max = 255)
    @Schema(type = "string", example = "your product description")
    private String description;

    @NotNull
    @Digits(integer = 10, fraction = 2)
    @Schema(type = "string", example = "1.00")
    private Double price;

    @NotNull
    @Schema(type = "string", example = "CAD")
    private Currency currency;
}
