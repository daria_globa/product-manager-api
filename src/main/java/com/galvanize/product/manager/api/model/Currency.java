package com.galvanize.product.manager.api.model;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.Optional;

@AllArgsConstructor
public enum Currency {

    @JsonEnumDefaultValue
    USD("USD"),
    CAD("CAD"),
    EUR("EUR"),
    GBP("GBP"),
    ;

    private final String name;

    public static Optional<Currency> byName(String cur) {
        return Arrays.stream(values())
                .filter(e -> e.name.equalsIgnoreCase(cur))
                .findFirst();
    }

    @Override
    public String toString() {
        return name;
    }
}
