package com.galvanize.product.manager.api.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NotificationSender {

    @Async(value = "subtask-executor")
    public void send(String msg) {
        throw new UnsupportedOperationException("No implemented yet");
    }
}
