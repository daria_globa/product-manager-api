package com.galvanize.product.manager.api.model.currency;

import lombok.Data;
import lombok.NoArgsConstructor;

public interface CurrencyModel {

    boolean succeed();

    Error error();

    @Data
    @NoArgsConstructor
    class Error {

        private static final int PLAN_RESTRICTION = 105;
        private static final int LIMIT_REACHED = 106;

        private int code;
        private String info;
        private Throwable cause;

        public Error(Throwable t) {
            this.cause = t;
        }
    }
}
