package com.galvanize.product.manager.api.exceptions;

import com.galvanize.product.manager.api.model.ErrorResponse;
import com.galvanize.product.manager.api.model.ErrorResponse.FieldError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.galvanize.product.manager.api.exceptions.ProductApiErrorCodes.*;


@Slf4j
@RestControllerAdvice(annotations = RestController.class)
public class GlobalExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleProductNotFound(final EntityNotFoundException e) {
        return new ResponseEntity<>(
                ErrorResponse.error(
                        PRODUCT_NOT_FOUND.errorCode,
                        e.getMessage()),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<ErrorResponse> handleNotFound(final ResponseStatusException e) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        PRODUCT_API_ERROR.errorCode,
                        PRODUCT_API_ERROR.info,
                        e.getMessage(),
                        Collections.emptyList()),
                e.getStatus());
    }

    @ExceptionHandler(value = {
            BindException.class,
            MethodArgumentNotValidException.class,
            MethodArgumentTypeMismatchException.class})
    public ResponseEntity<ErrorResponse> handleBadRequestParameters(final Throwable e) {
        List<FieldError> fieldErrors;
        if (e instanceof BindException) {
            final BindingResult bindingResult = ((BindException) e).getBindingResult();

            fieldErrors = bindingResult.getFieldErrors()
                    .stream()
                    .map(error -> new FieldError(
                            error.getField(),
                            error.getCode(),
                            error.getRejectedValue()))
                    .collect(Collectors.toList());

        } else {
            final MethodArgumentTypeMismatchException me = ((MethodArgumentTypeMismatchException) e);
            fieldErrors = Collections.singletonList(new FieldError(
                    me.getName(),
                    null,
                    String.valueOf(me.getValue())));
        }

        return new ResponseEntity<>(
                new ErrorResponse(
                        NOT_VALID.errorCode,
                        NOT_VALID.info,
                        null,
                        fieldErrors),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageConversionException.class)
    public ResponseEntity<ErrorResponse> handleJsonConversionException(final HttpMessageConversionException e) {
        log.error("Failed to parse json: " + e.getMessage(), e.getRootCause());
        return new ResponseEntity<>(
                new ErrorResponse(
                        NOT_VALID.errorCode,
                        NOT_VALID.info,
                        "json parse error",
                        Collections.emptyList()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ErrorResponse> handleThrowable(final Throwable e) {
        log.error("Unexpected error: " + e.getMessage(), e);
        return new ResponseEntity<>(
                new ErrorResponse(
                        GENERAL_ERROR.errorCode,
                        GENERAL_ERROR.info,
                        e.getMessage(),
                        Collections.emptyList()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
