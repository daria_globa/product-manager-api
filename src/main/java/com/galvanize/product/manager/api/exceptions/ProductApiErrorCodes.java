package com.galvanize.product.manager.api.exceptions;

public enum ProductApiErrorCodes {

    PRODUCT_NOT_FOUND(1331, "Product not found"),
    NOT_VALID(3443, "Value not valid"),
    PRODUCT_API_ERROR(5665, "Product API error"),
    GENERAL_ERROR(9999, "Service unavailable"),

    ;

    public final int errorCode;
    public final String info;

    ProductApiErrorCodes(int value, String info) {
        this.errorCode = value;
        this.info = info;
    }
}
