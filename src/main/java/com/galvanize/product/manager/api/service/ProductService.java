package com.galvanize.product.manager.api.service;

import com.galvanize.product.manager.api.domain.Product;
import com.galvanize.product.manager.api.exceptions.CurrencyServiceException;
import com.galvanize.product.manager.api.model.Currency;
import com.galvanize.product.manager.api.model.ProductDTO;
import com.galvanize.product.manager.api.repository.ProductRepository;
import com.galvanize.product.manager.api.service.util.ProductMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static com.galvanize.product.manager.api.service.util.ProductMapper.convert;


@Slf4j
@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final CurrencyService currencyService;

    private final Optional<NotificationSender> notificationSender;

    public ProductService(final ProductRepository productRepository,
                          final CurrencyService currencyService,
                          final NotificationSender notificationSender) {
        this.productRepository = productRepository;
        this.currencyService = currencyService;
        this.notificationSender = Optional.ofNullable(notificationSender);
    }

    public ProductDTO create(final ProductDTO productDTO) {
        Integer id = productRepository.save(convert(productDTO)).getId();
        productDTO.setId(id);
        return productDTO;
    }

    public void delete(final Integer id) {
        /*
            Product delete doesn't really remove corresponding record in the db,
            instead, it updates Product.deleted column with 'true' and therefore
            hides the record from further usage.
         */
        productRepository.deleteById(id);
        log.debug("Product with id={} has been deleted", id);
    }

    public ProductDTO get(Integer productId, Optional<Currency> toCurrency) {
        /*
            EntityNotFoundException might be thrown by the DAO layer natively,
            but Repository.findById() is replaced by the implementation using
            'Product.deleted' flag as a second query parameter.

            Now, receiving Optional.empty(), we explicitly throw the exception,
            which will be intercepted by the global exception handler. */
        final Product rawProduct = productRepository
                .findById(productId)
                .orElseThrow(() -> new EntityNotFoundException("Product with id=" + productId + " not found"));

        productRepository.incrementProductViews(productId);

        return toCurrency
                .map(to -> convertPriceCurrency(rawProduct.getPrice(), Currency.USD, to))
                .filter(price -> price != rawProduct.getPrice())
                .map(convertedPrice -> {
                    final ProductDTO product = ProductMapper.convert(rawProduct);
                    product.setPrice(convertedPrice);
                    product.setCurrency(toCurrency.get());
                    return product;
                })
                .orElseGet(() -> ProductMapper.convert(rawProduct));
    }

    protected double convertPriceCurrency(double price, Currency from, Currency to) {
        try {
            if (from == to) return price;
            return currencyService.convert(price, from, to);
        } catch (CurrencyServiceException e) {
            log.error("Failed to convert currency via Currency service", e);
            notificationSender
                    .ifPresent(s -> s.send(String.format("Failed to convert price from %s to %s", from, to)));
            return price;
        }
    }
}
