package com.galvanize.product.manager.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    @GetMapping("/")
    @ResponseBody
    public String index() {
        return "Product Manager Service welcomes you buddy!\n" +
                "To use API go to /api/product.*\n" +
                "  * API info is here: /swagger-ui-custom.html. ";
    }
}
