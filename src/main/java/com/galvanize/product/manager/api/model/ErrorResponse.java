package com.galvanize.product.manager.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {

    private Integer errorCode;
    private String exceptionId;
    private String message;
    private List<FieldError> fieldErrors;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class FieldError {

        private String field;
        private String error;
        private Object value;
    }

    public static ErrorResponse error(Integer errorCode, String msg) {
        return new ErrorResponse(errorCode, null, msg, Collections.emptyList());
    }
}
