package com.galvanize.product.manager.api.service;

import com.galvanize.product.manager.api.exceptions.CurrencyServiceException;
import com.galvanize.product.manager.api.model.Currency;
import com.galvanize.product.manager.api.model.currency.CurrencyConversion;
import com.galvanize.product.manager.api.model.currency.CurrencyQuotes;
import lombok.Builder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;

/*
    It's needed to implement more tests, right now the following cases are tested:
    - conversion failed, when both endpoints are unavailable;
    - conversion failed, when both responded with 'success = false'
    - conversion succeed, when /convert failed, but /quotes responded with rates


    Testing approach: to reach the goal of having unit tests without struggling with
    pain of mocking Spring's WebClient, it's suggested extending CurrencyService,
    overriding CurrencyService.convertDirectly() and CurrencyService.convertThroughQuotesRequest()
    while leaving the main method CurrencyService.convert() as is.
 */
class CurrencyServiceTest {

    @Test
    public void throw_CurrencyServiceException_when_bothEndpointsUnavailable() {
        final CurrencyService s = CurrencyServiceWebMock
                .builder()
                .convertUrlUnavailable(true)
                .liveUrlUnavailable(true)
                .build();

        Assertions.assertThrows(
                CurrencyServiceException.class,
                () -> s.convert(10d, Currency.CAD, Currency.EUR));
    }

    @Test
    public void throw_CurrencyServiceException_when_bothEndpointsFailedWithError() {
        final CurrencyService s = CurrencyServiceWebMock
                .builder()
                .conversionFailedWithError(true)
                .liveFailedWithError(true)
                .build();

        Assertions.assertThrows(
                CurrencyServiceException.class,
                () -> s.convert(10d, Currency.CAD, Currency.EUR));
    }

    @Test
    public void throw_CurrencyServiceException_when_bothEndpointsFailedWithoutError() {
        final CurrencyService s = CurrencyServiceWebMock
                .builder()
                .conversionSucceed(false)
                .liveSucceed(false)
                .build();

        Assertions.assertThrows(
                CurrencyServiceException.class,
                () -> s.convert(10d, Currency.CAD, Currency.EUR));
    }

    @Test
    public void convert_usingLiveEndpoint_when_convertEndpointUnavailable() {
        final CurrencyService s = CurrencyServiceWebMock
                .builder()
                .convertUrlUnavailable(true)
                .liveSucceed(true)
                .build();

        final double price = 10.0d;
        Assertions.assertEquals(
                s.convert(price, Currency.USD, Currency.CAD),
                price * CurrencyServiceWebMock.liveSuccess.quote(Currency.CAD));
    }

    @Builder
    private static final class CurrencyServiceWebMock extends CurrencyService {

        static final CurrencyConversion convertSuccess = new CurrencyConversion(
                true,
                100.0d,
                null);
        static final CurrencyQuotes liveSuccess = new CurrencyQuotes(
                true,
                Currency.USD,
                Collections.singletonMap(Currency.CAD, 3.2D),
                null);

        private boolean convertUrlUnavailable;
        private boolean conversionSucceed;
        private boolean conversionFailedWithError;

        private boolean liveUrlUnavailable;
        private boolean liveSucceed;
        private boolean liveFailedWithError;

        public CurrencyServiceWebMock(boolean convertUrlUnavailable,
                                      boolean conversionSucceed,
                                      boolean conversionFailedWithError,
                                      boolean liveUrlUnavailable,
                                      boolean liveSucceed,
                                      boolean liveFailedWithError) {
            super(null);
            this.convertUrlUnavailable = convertUrlUnavailable;
            this.conversionSucceed = conversionSucceed;
            this.conversionFailedWithError = conversionFailedWithError;
            this.liveUrlUnavailable = liveUrlUnavailable;
            this.liveSucceed = liveSucceed;
            this.liveFailedWithError = liveFailedWithError;
        }

        @Override
        protected CurrencyConversion convertDirectly(double amount, Currency from, Currency to) {
            if (convertUrlUnavailable)
                return CurrencyConversion.error(new IllegalArgumentException("conversion unavailable"));

            if (conversionSucceed)
                return convertSuccess;

            if (conversionFailedWithError)
                return CurrencyConversion.error(new IllegalArgumentException("conversion failed"));

            return new CurrencyConversion(false, 0, null);
        }

        @Override
        protected CurrencyQuotes convertThroughQuotesRequest(Currency from, Currency to) {
            if (liveUrlUnavailable)
                return CurrencyQuotes.error(new IllegalArgumentException("conversion unavailable"));

            if (liveSucceed)
                return liveSuccess;

            if (conversionFailedWithError)
                return CurrencyQuotes.error(new IllegalArgumentException("conversion failed"));

            return new CurrencyQuotes(false, null, null, null);
        }
    }
}