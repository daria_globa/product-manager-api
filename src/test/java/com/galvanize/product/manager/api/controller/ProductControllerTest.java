package com.galvanize.product.manager.api.controller;

import com.galvanize.product.manager.api.exceptions.GlobalExceptionHandler;
import com.galvanize.product.manager.api.model.ErrorResponse;
import com.galvanize.product.manager.api.model.ProductDTO;
import com.galvanize.product.manager.api.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.Optional;

import static com.galvanize.product.manager.api.exceptions.ProductApiErrorCodes.GENERAL_ERROR;
import static com.galvanize.product.manager.api.exceptions.ProductApiErrorCodes.PRODUCT_NOT_FOUND;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/*
    It's needed to implement more tests, right now the following cases are tested:
    - receiving OK when product exists
    - receiving NOT_FOUND when product doesn't exist
    - receiving SERVER_ERROR when unexpected error happened

    Testing approach: pure mocking test.

    However, error tests are implemented with 'dirty' hack.
    Since ErrorHandler in the application is applied though the interception mechanism,
    it's needed to launch the app context to make it work.

    In this case we want to avoid introducing Spring context because
    it makes tests rather integration / component level than unit. That's why, the exception
    handler is initialized directly and handles the given error, which means
    that the controller's tests are also the handler's tests, which is not perfect, but still acceptable
    because the higher level tests will test the contract as well.
 */
class ProductControllerTest {

    ProductController controller;
    ProductService service;

    @BeforeEach
    void init() {
        service = Mockito.mock(ProductService.class);
        controller = new ProductController(service);
    }

    @Test
    public void OK_and_product_when_productExists() {
        final ProductDTO expected = new ProductDTO();
        when(service.get(any(Integer.class), any(Optional.class))).thenReturn(expected);

        final ResponseEntity<ProductDTO> resp = controller.getProduct(1, null);

        assertEquals(resp.getStatusCode(), HttpStatus.OK);
        assertEquals(resp.getBody(), expected);
    }

    @Test
    public void NOT_FOUND_when_productDoesNotExist() {
        final GlobalExceptionHandler handler = new GlobalExceptionHandler();

        when(service.get(any(Integer.class), any(Optional.class))).thenThrow(new EntityNotFoundException());

        try {
            controller.getProduct(1, null);
            fail("Exception is not thrown! Configure mocks");
        } catch (EntityNotFoundException e) {
            ErrorResponse expected = ErrorResponse.error(PRODUCT_NOT_FOUND.errorCode, e.getMessage());

            ResponseEntity<ErrorResponse> resp = handler.handleProductNotFound(e);

            assertEquals(resp.getStatusCode(), HttpStatus.NOT_FOUND);
            assertEquals(resp.getBody(), expected);
        } catch (Throwable t) {
            fail("Received unexpected error. Check mocks configs.");
        }
    }

    @Test
    public void SERVER_ERROR_when_unexpectedError() {
        final GlobalExceptionHandler handler = new GlobalExceptionHandler();

        when(service.get(any(Integer.class), any(Optional.class))).thenThrow(new IllegalArgumentException());

        try {
            controller.getProduct(1, null);
            fail("Exception is not thrown! Configure mocks");
        } catch (Throwable t) {
            ErrorResponse expected = new ErrorResponse(
                    GENERAL_ERROR.errorCode,
                    GENERAL_ERROR.info,
                    t.getMessage(),
                    Collections.emptyList());

            ResponseEntity<ErrorResponse> resp = handler.handleThrowable(t);

            assertEquals(resp.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
            assertEquals(resp.getBody(), expected);
        }
    }
}