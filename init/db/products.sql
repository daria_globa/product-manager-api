\c prodmandb;
INSERT INTO product(name, price, currency) VALUES  ('prod1', 100.0,  'CAD');
INSERT INTO product(name, price, currency) VALUES  ('prod2', 0.5,    'USD');
INSERT INTO product(name, price, currency) VALUES  ('prod3', 0.008,  'CAD');
INSERT INTO product(name, price, currency) VALUES  ('prod7', 1555.3, 'EUR');
INSERT INTO product(name, price, currency) VALUES  ('prod9', 8.07,   'GBP');
