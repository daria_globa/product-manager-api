CREATE DATABASE prodmandb;
\c prodmandb;
CREATE sequence primary_sequence start 1 increment 1;
CREATE TABLE product (
     id             BIGINT          DEFAULT nextval('primary_sequence'::regclass) UNIQUE NOT NULL,
     name           VARCHAR(50)     NOT NULL,
     description    VARCHAR(255),
     price          NUMERIC(10, 2)  NOT NULL,
     currency       CHAR(3)         NOT NULL,
     views_count    INT             NOT NULL DEFAULT 0,
     deleted        BOOLEAN         NOT NULL DEFAULT false);